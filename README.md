# joker_dyndns

Simple script to update dynamic A and AAAA records at [Joker.com](https://joker.com), following their [What is Dynamic DNS (DynDNS)?](https://joker.com/faq/content/11/427/en/what-is-dynamic-dns-dyndns.html?highlight=dyndns) FAQ.

## Preparation

1. Log in to https://joker.com and generate a `USERNAME` and `PASSWORD` set for Dynamic DNS.
2. Create a DYNA and a DYNAAAA record for your `DOMAIN`
3. Copy [update.conf.template](./update.conf.template) into the to-be-created `update.conf` file and enter your data for `USERNAME`, `PASSWORD`, and `DOMAIN`
4. Make the script executable


## Usage

After the preparation, simply call 

    ./update.sh update.conf

or setup a cronjob calling the script every 10 minutes, for example, via an entry in `crontab`:

    */10 * * * * /my/path/to/update.sh /my/path/to/update.conf

As the script always updates the `LAST_CALL=` entry in `update.conf` you can check the timestamp on that file to get the last time the script was called.

Additionally, in the config file two variables are set indicating the last time the script detected a change in the IP addresses: `LAST_CHANGE_IPV4` and `LAST_CHANGE_IPV6`, respectively.


## Authors and acknowledgment
The [getip-from-fritzbox.sh](./get-ip-from-fritzbox.sh) script is taken from  http://scytale.name/blog/2010/01/fritzbox-wan-ip and released there under a [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) license.

## License
GPL v3, see [LICENSE](./LICENSE).
