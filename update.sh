#!/bin/bash

CONFIG_FILE=$1

get_current_ipv4_from_fritzbox() {
    # All credits for this one liner go to the author of this blog:
    # http://scytale.name/blog/2010/01/fritzbox-wan-ip
    # As the author explains its not required to tamper with the provided IP for the FritzBox
    # as it always binds to that address for UPnP.
    CURRENT_IPV4=$(
        curl -s -H 'Content-Type: text/xml; charset="utf-8"' \
        -H 'SOAPAction: urn:schemas-upnp-org:service:WANIPConnection:1#GetExternalIPAddress' \
        -d '<?xml version="1.0" encoding="utf-8"?><s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"> <s:Body> <u:GetExternalIPAddress xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1" /></s:Body></s:Envelope>' \
        'http://fritz.box:49000/igdupnp/control/WANIPConn1' | \
        grep -Eo '\<[[:digit:]]{1,3}(\.[[:digit:]]{1,3}){3}\>'
    )
}

get_current_ipv4_from_external() {
    CURRENT_IPV4=$(curl -4 https://svc.joker.com/nic/myip --silent)
}

get_current_ipv6_from_external() {
    CURRENT_IPV6=$(curl -6 https://svc.joker.com/nic/myip --silent)
}

update_dyn_record() {
    local CACHE_VARNAME=CACHED_IPV$1
    local CACHED_IP=${!CACHE_VARNAME}

    local CURRENT_VARNAME=CURRENT_IPV$1
    local CURRENT_IP=${!CURRENT_VARNAME}

    # Joker's update URL:
    # https://svc.joker.com/nic/update?username=<username>&password=<pass>&myip=<ipaddr>&hostname=<domain>
    #
    # Joker.com will set the DYNA record if myip is a IPv4 and the DYNAAAA record if it's a IPv6
    local URL="https://svc.joker.com/nic/update?username=$USERNAME&password=$PASSWORD&myip=$CURRENT_IP&hostname=$DOMAIN"

    # Check if IPV4 update is needed and if yes, trigger update and cache current IPV4
    if [[ $CACHED_IP != $CURRENT_IP ]]; then
        JOKER_REPLY=$(curl $URL --silent)
        if grep '^good.*' <<< $JOKER_REPLY; then
            sed -i s/"LAST_CHANGE_IPV$1.*"/"LAST_CHANGE_IPV$1=$(date --iso-8601=s)"/ $CONFIG_FILE
            sed -i s/"CACHED_IPV$1.*"/"CACHED_IPV$1=$CURRENT_IP"/ $CONFIG_FILE
            echo "updated $CURRENT_IP"
        else
            echo "$JOKER_REPLY"
        fi
    else
        echo "cached $CURRENT_IP"
    fi
}


# load paramaters from .env file
if [[ -f $CONFIG_FILE ]]; then
    source $CONFIG_FILE
else
    echo "No config file found --> exiting."
    echo "call this script as: $0 <path-to-config-file> " 
    exit
fi

# Get current IP values
#get_current_ipv4_from_external
get_current_ipv4_from_fritzbox
get_current_ipv6_from_external


# Check if update is needed and if yes, trigger update, and cache current
update_dyn_record 4
update_dyn_record 6

sed -i s/"LAST_CALL=.*"/"LAST_CALL=$(date --iso-8601=s)"/ $CONFIG_FILE